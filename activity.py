name = "Jose"
age = 18
occupation = "writer"
movie = "One More Chance"
rating = 99.6

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

num1, num2, num3 = 3, 50, 75

print(num1*num2)
print(num1 <= num3)
print(num3+num2)

# Specifications
# 1. Create 5 variables and output them in the command prompt in the following format:
# "I am < name (string)> , and I am < age (integer)> years old, I work as a < occupation (string)> , and my rating for < movie (string)> is < rating (decimal)> %"
name = "Jose"
age = 38
occupation = "writer"
movie = "One more chance"
rating = 99.6

print(f"I am {name} , and I am {age} years old, I work as a {occupation} , and my rating for {movie} is {rating} %")

# 2. Perform the following statements
# Create 3 variables, num1, num2, and num3
num1 = 10
num2 = 15
num3 = 12
# Get the product of num1 and num2
print(num1 * num2)
# Check if num1 is less than num3
print(num1 < num3)
# Add the value of num3 to num2
num2 += num3
